import datetime

from miflora.miflora_poller import MiFloraPoller, \
    MI_CONDUCTIVITY, MI_BATTERY, MI_MOISTURE, MI_LIGHT, MI_TEMPERATURE
from btlewrap.bluepy import BluepyBackend

from db.helper import save_log

BT_ADDRESS = 'C4:7C:8D:61:76:5F'
poller = MiFloraPoller(BT_ADDRESS, BluepyBackend)

try:
    # device data
    firmware = poller.firmware_version()
    battery = poller.parameter_value(MI_BATTERY)

    # sensor data
    moisture = poller.parameter_value(MI_MOISTURE)
    conductivity = poller.parameter_value(MI_CONDUCTIVITY)
    light = poller.parameter_value(MI_LIGHT)
    temperature = poller.parameter_value(MI_TEMPERATURE)

    # print to console
    print('BT Address', BT_ADDRESS)
    print('Firmware', firmware)
    print('Battery', battery)

    print('Moisture', moisture)
    print('Conductivity', conductivity)
    print('Light', light)
    print('Temperature', temperature)

    timestamp = str(datetime.datetime.now())

    # save locally
    save_log(source=BT_ADDRESS, firmware=firmware, battery=battery,
             moisture=moisture, conductivity=conductivity,
             light=light, temperature=temperature, timestamp=timestamp)

    print("Saved to DB at", timestamp)

except Exception as e:
    print('Error', e)



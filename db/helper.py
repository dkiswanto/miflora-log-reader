import sqlite3

from setup import DB_NAME

conn = sqlite3.connect(DB_NAME)
conn.row_factory = sqlite3.Row
local_cursor = conn.cursor()

NOT_SYNC = 0
SYNC = 1


def save_log(**kwargs):
    attributes = ', '.join(kwargs.keys())
    values = ', '.join(map(values_formatter,kwargs.values()))
    query = 'INSERT INTO miflora_log (%s, sync) VALUES (%s, %s)' % (attributes, values, NOT_SYNC)
    print(query)
    local_cursor.execute(query)
    conn.commit()


def get_unsync_log():
    query = 'SELECT * FROM miflora_log WHERE sync == 0'
    logs = local_cursor.execute(query)
    return logs


def set_sync_log(ids):
    query = 'UPDATE miflora_log SET sync = 1 WHERE id in ({})'.format(', '.join(map(str,ids)))
    local_cursor.execute(query)
    conn.commit()
    return True


def values_formatter(val):
    if isinstance(val, str):
        return "'{}'".format(val)
    else:
        return str(val)
